#include <iostream>

#include "Field.h"
#include "Fields.h"
#include "Game.h"

int main() {
	Checkers::Dimension dim = 10;
	std::vector<std::string> direct { "NorthEast", "SouthEast", "SouthWest",
			"NorthWest" };

	Checkers::Fields fields { dim };
	for (uint8_t i = 0; i < fields.getSize(); i++) {
		uint8_t fieldId = i + 1;
		std::cout << "field.id: " << static_cast<unsigned>(fieldId) << std::flush;

		for (uint8_t i = 0; i < 4; i++) {
			uint8_t neighborId = fields.getNeighborId(fieldId,
					static_cast<Checkers::Direction>(i));
			if (neighborId != 0) {
				std::cout << ", " << direct[i] << ": " << static_cast<unsigned>(neighborId)
						<< std::flush;
			}
		}

		std::cout << '\n';
	}
	Checkers::Game g{fields};
	std::cout << "Fields address [main]: " << &fields << '\n';
	g.showPaws();
	//createPaws();

}
