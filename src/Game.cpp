#include <iostream>
#include "Game.h"
#include "FieldOccupyFactory.h"

namespace Checkers {
Game::Game(Fields &outer_fields) :
		fields { outer_fields }, white { FieldOccupyFactory::createFieldOccupy(
				outer_fields.getDimension(), Color::white) }, black {
				FieldOccupyFactory::createFieldOccupy(
						outer_fields.getDimension(), Color::black) } {

}

Game::~Game() {
	//dtor
}
void Game::showPaws() const {
	std::cout << "Fields address [Game]: " << &fields << '\n';
	std::cout << "White paws occupy:\n";
	for (uint8_t i = 0; i < white.pawns_on_field.size(); i++) {
		std::cout << ": " << int(white.pawns_on_field[i]) << '\n';
	}
	std::cout << "Black paws occupy:\n";
	for (uint8_t i = 0; i < black.pawns_on_field.size(); i++) {
		std::cout << ": " << int(black.pawns_on_field[i]) << '\n';
	}

}
}
