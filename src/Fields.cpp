/*
 * Fields.cpp
 *
 *  Created on: Jan 14, 2019
 *      Author: nasz
 */
#include <stdexcept>
#include <sstream>
#include "Field.h"
#include "Fields.h"

namespace Checkers {
using Index = uint16_t;
typedef std::basic_ostringstream<char> ostringstream;
Fields::Fields(Dimension dimention) :
		dimention { dimention } {
	validateDimension();
	fields.reserve(dimention * dimention / 2);
	for (Position i = 0; i < dimention * dimention / 2; i++) {
		fields.emplace_back(i + 1);
	}

	for (ColumnNumber column = 0; column < dimention / 2; ++column) {
		for (RowNumber row = 0; row < dimention; ++row) {
			int element = (row * dimention / 2) + column + 1;
			if (row % 2 == 0) {
				populateFieldsForEvenRow(element, row);
			} else {
				populateFieldsForOddRow(element, row);
			}
		}
	}
}

Fields::~Fields() {
	// TODO Auto-generated destructor stub
}
void Fields::validateDimension() const {
	if (dimention > MAX_DIMENSON) {
		ostringstream message;
		message << "Given dimension " << int(dimention)
				<< " exceeds max dimension " << int(MAX_DIMENSON);
		throw std::invalid_argument(message.str());
	}
	if (dimention % 2 == 1) {
		ostringstream message;
		message << "Given dimension " << int(dimention)
				<< " is not even";
		throw std::invalid_argument(message.str());
	}
}
void Fields::populateFieldsForEvenRow(Position element, RowNumber row) {
	Dimension dim_half = dimention / 2;
	DirectionForPosition direct;

	direct[Direction::NorthWest] = element - dim_half;
	direct[Direction::NorthEast] = element - dim_half + 1;
	direct[Direction::SouthWest] = element + dim_half;
	direct[Direction::SouthEast] = element + dim_half + 1;

	if (static_cast<PositionWithNegative>(direct[Direction::SouthEast]
			- dim_half * (row + 2)) > 0) {
		direct[Direction::SouthEast] = 0;
	}
	if (static_cast<PositionWithNegative>(direct[Direction::NorthEast]
			- dim_half * row) > 0) {
		direct[Direction::NorthEast] = 0;
	}
	populateFields(fields[element - 1], direct);
}
void Fields::populateFieldsForOddRow(Position element, RowNumber row) {
	Dimension dim_half = dimention / 2;
	DirectionForPosition direct;

	direct[Direction::NorthWest] = element - dim_half - 1;
	direct[Direction::NorthEast] = element - dim_half;

	direct[Direction::SouthWest] = element + dim_half - 1;
	direct[Direction::SouthEast] = element + dim_half;

	if (static_cast<PositionWithNegative>(direct[Direction::SouthWest]
			- dim_half * (row + 1)) == 0) {
		direct[Direction::SouthWest] = 0;
	}
	if (static_cast<PositionWithNegative>(direct[Direction::NorthWest]
			- dim_half * (row - 1)) == 0) {
		direct[Direction::NorthWest] = 0;
	}
	populateFields(fields[element - 1], direct);
}
void Fields::populateFields(Field& element, DirectionForPosition& direct) {
	Dimension max_fields = dimention * dimention / 2;
	for (Index i = 0; i < direct.size(); ++i) {
		if (direct[i] <= max_fields && direct[i] > 0) {
			Field* pointer = &fields[direct[i] - 1];
			element.neighbors[static_cast<Direction>(i)] = pointer;
		}
	}
}
Position Fields::getNeighborId(const Position baseId,
		const Direction direction) const {
	try {
		const Field& field = fields.at(baseId - 1);
		const Field* neighbor = field.neighbors.at(direction);
		return neighbor->id;
	} catch (const std::out_of_range& oor) {
		return 0;
	}
}
}  // namespace Checkers
