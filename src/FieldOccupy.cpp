/*
 * FieldOccupy.cpp
 *
 *  Created on: 5 mar 2019
 *      Author: dazar1
 */

#include "FieldOccupy.h"
#include "Field.h"

namespace Checkers {
using Index = uint64_t;
FieldOccupy::FieldOccupy(PawsOccupyStruct& rawPositions) {
	encode(rawPositions);
}

PawsOccupyStruct FieldOccupy::decode() {
	PawsOccupyStruct rawPawsPosition { 0, 0 };
	for (Index i = 0; i < pawns_on_field.size(); ++i) {
		rawPawsPosition.paws[i - 1] = 1;
	}
	for (Index i = 0; i < queens_on_field.size(); ++i) {
		rawPawsPosition.queens[i - 1] = 1;
	}
	return rawPawsPosition;
}
void FieldOccupy::encode(PawsOccupyStruct& rawPositions) {
	for (Position i = 0; i < rawPositions.paws.size(); ++i) {
		if (rawPositions.paws[i] == 1) {
			pawns_on_field.push_back(i + 1);
		}
	}
	for (Position i = 0; i < rawPositions.queens.size(); ++i) {
		if (rawPositions.queens[i] == 1) {
			queens_on_field.push_back(i + 1);
		}
	}
}

void FieldOccupy::placePositionOfCheckers(PawsOccupyStruct& rawPositions) {
	pawns_on_field.clear();
	queens_on_field.clear();
}
}
