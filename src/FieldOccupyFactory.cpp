/*
 * FieldOccupyFactory.cpp
 *
 *  Created on: 24 mar 2019
 *      Author: dazar1
 */
#include <cmath>
#include <sstream>

#include "FieldOccupyFactory.h"
#include "FieldOccupy.h"

namespace Checkers {
using PawsNumber = uint8_t;
typedef std::basic_ostringstream<char> ostringstream;
FieldOccupy FieldOccupyFactory::createFieldOccupy(Dimension dimention,
		Color color) {
	PawsOccupyStruct rawPawsPosition { 0, 0 };
	checkMaxDimention(dimention, rawPawsPosition);
	PawsNumber pawsNumber = (dimention / 2) * (dimention / 2 - 1);
	Position start = calculateStart(dimention, color);
	for (Position i = start; i < (start + pawsNumber); ++i) {
		rawPawsPosition.paws[i] = 1;
	}
	return FieldOccupy(rawPawsPosition);
}
Position FieldOccupyFactory::calculateStart(Dimension dim, Color color) {
	Position firstWhite = (dim / 2) * (dim / 2 + 1) - 1;
	if (color == Color::black) {
		return 0;
	} else if (color == Color::white) {
		return firstWhite + 1;
	} else {
		ostringstream message;
		message << "Unknown Color " << color;
		throw std::invalid_argument(message.str());
	}
}
void FieldOccupyFactory::checkMaxDimention(Dimension dimention,
		PawsOccupyStruct& rawPawsPosition) {
	Dimension max_dimension = static_cast<Dimension>(sqrt(
			rawPawsPosition.paws.size() * 2));
	if (dimention > max_dimension) {
		ostringstream message;
		message << "Given dimension " << int(dimention)
				<< " exceeds max dimension " << int(max_dimension);
		throw std::invalid_argument(message.str());
	}
}
}
