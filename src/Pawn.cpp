#include "Pawn.h"

Pawn::Pawn() {
	//ctor
}

Pawn::~Pawn() {
	//dtor
}

Pawn::Pawn(const Pawn& other) {
	//copy ctor
}

Pawn& Pawn::operator=(const Pawn& rhs) {
	if (this == &rhs)
		return *this; // handle self assignment
	//assignment operator
	return *this;
}
