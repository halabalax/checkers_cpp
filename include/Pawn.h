#ifndef PAWN_H
#define PAWN_H

class Pawn {
public:
	Pawn();
	virtual ~Pawn();
	Pawn(const Pawn& other);
	Pawn& operator=(const Pawn& other);

protected:

private:
};

class BlackPawn : public Pawn {

};

class WhitePawn : public Pawn {

};

class WhiteQueen: public WhitePawn {

};
class BlackQueen: public BlackPawn {

};

#endif // PAWN_H
