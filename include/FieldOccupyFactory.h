/*
 * FieldOccupyFactory.h
 *
 *  Created on: 24 mar 2019
 *      Author: dazar1
 */

#ifndef INCLUDE_FIELDOCCUPYFACTORY_H_
#define INCLUDE_FIELDOCCUPYFACTORY_H_

#include "FieldOccupy.h"

namespace Checkers {
enum Color {
	black, white
};
class FieldOccupyFactory {
public:
	static FieldOccupy createFieldOccupy(Dimension dimention, Color color);
private:
	static Position calculateStart(Dimension dim, Color color);
	static void checkMaxDimention(Dimension dimention,
			PawsOccupyStruct& rawPawsPosition);
};
}

#endif /* INCLUDE_FIELDOCCUPYFACTORY_H_ */
