#ifndef _FIELD_H
#define _FIELD_H

#include <iostream>
#include <unordered_map>
#include <cstdint>

namespace Checkers {
using Id = uint8_t;
enum Direction {
	NorthEast = 0, SouthEast = 1, SouthWest = 2, NorthWest = 3
};
class Field {
	friend class Fields;
public:
	Field(Id id) :
			id { id } {
	}
	virtual ~Field();
	Field(const Field& other) = delete;
	Field(Field&& other) = default;
	Field& operator=(const Field& other) = delete;
//private:
	Id id;
	std::unordered_map<Direction, Field*> neighbors { };
};
}

#endif // _FIELD_H
