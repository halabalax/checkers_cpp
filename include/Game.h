#ifndef GAME_H
#define GAME_H

#include "Field.h"
#include "Fields.h"
#include "FieldOccupy.h"
#include "Pawn.h"

namespace Checkers {
using Index = uint64_t;
class Game {
public:
	Game(Fields& fields);
	virtual ~Game();
	Game(const Game &other) = delete;
	Game& operator=(const Game &other) = delete;

	void showPaws() const;
protected:

private:
	const Fields& fields;
	FieldOccupy white;
	FieldOccupy black;

	void createFields();
	void createBlackPawns();
	void createWhitePawns();
};
}

#endif // GAME_H
