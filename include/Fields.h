#ifndef FIELDS_H_
#define FIELDS_H_

#include <vector>

#include "Field.h"

namespace Checkers {
using Position = uint8_t;
using PositionWithNegative = int16_t;
using Dimension = uint8_t;
using RowNumber = uint8_t;
using ColumnNumber = uint8_t;
using Size = uint8_t;
using DirectionForPosition = std::array<PositionWithNegative, 4>;
class Fields {
public:
	Fields(Dimension dimention);
	virtual ~Fields();
	Fields(const Fields& other) = delete;
	Fields& operator=(const Fields& other) = delete;

	Position getNeighborId(const Position baseId,
			const Direction direction) const;
	Size getSize() const {
		return fields.size();
	}
	Dimension getDimension() const {
		return dimention;
	}
private:
	void validateDimension() const;
	void populateFieldsForEvenRow(Position element, RowNumber row);
	void populateFieldsForOddRow(Position element, RowNumber row);
	void populateFields(Field& element,
			DirectionForPosition& directions);
	const Dimension dimention;
	std::vector<Field> fields;

	static const Dimension MAX_DIMENSON = 10;
};
}

#endif /* FIELDS_H_ */
