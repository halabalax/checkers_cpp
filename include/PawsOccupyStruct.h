/*
 * PawsOccupyStruct.h
 *
 *  Created on: 5 mar 2019
 *      Author: dazar1
 */

#ifndef INCLUDE_PAWSOCCUPYSTRUCT_H_
#define INCLUDE_PAWSOCCUPYSTRUCT_H_

#include <cstdint>
#include <bitset>

namespace Checkers {
struct PawsOccupyStruct {
	std::bitset<72> paws;
	std::bitset<72> queens;
};
}

#endif /* INCLUDE_PAWSOCCUPYSTRUCT_H_ */
