#ifndef FIELD_OCCUPY_H
#define FIELD_OCCUPY_H

#include <vector>
#include <cstdint>
#include "Fields.h"
#include "PawsOccupyStruct.h"

namespace Checkers {
class FieldOccupy {
public:
	FieldOccupy(PawsOccupyStruct& rawPositions);
	PawsOccupyStruct decode();

private:
	void checkMaxDimention(Dimension dimention,
			PawsOccupyStruct& rawPawsPosition);
	void encode(PawsOccupyStruct& rawPositions);
	void placePositionOfCheckers(PawsOccupyStruct& rawPositions);
public:
	std::vector<Position> pawns_on_field;
	std::vector<Position> queens_on_field;
};
}
#endif // FIELD_OCCUPY_H
